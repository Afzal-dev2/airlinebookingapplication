
# Airline Ticket Booking Application

This is a Java application for managing airline ticket bookings. It allows users to insert passenger details, insert flight details, book tickets, cancel tickets, update passenger information, update flight details, and display vacant seat details.

## Features

- Insert Passenger Details
- Insert Flight Details
- Book Ticket
- Cancel Ticket
- Update Passenger Information
- Update Flight Details
- Display Vacant Seat Details

## Getting Started

To run the application, you need:

- Java Development Kit (JDK) installed on your system
- MySQL database installed and running
- GitLab repository cloned to your local machine

## Installation

Clone the repository to your local machine:

```bash
git clone https://gitlab.com/afzal-dev2/airline-ticket-booking.git
```

Compile the Java files:

```bash
javac *.java
```

Run the application:

```bash
java AirlineTicketBookingApp
```

## Usage

Follow the on-screen prompts to perform various operations such as inserting passenger details, inserting flight details, booking tickets, etc.

## Documentation

- `AirlineTicketBookingApp.java`: Main class for running the application and handling user interactions. This class serves as the entry point for the application. It provides a menu-driven interface for users to interact with the booking system.

- `PassengerManager.java`: Manages operations related to passengers such as inserting passenger details, updating passenger information, etc. This class handles the management of passenger data including inserting new passengers and updating existing passenger information.

- `FlightManager.java`: Manages operations related to flights such as inserting flight details, updating flight information, etc. This class is responsible for managing flight-related operations such as inserting new flights, updating flight information, and displaying vacant seat details.

- `BookingManager.java`: Manages operations related to bookings such as booking tickets, canceling tickets, etc. This class handles the booking and cancellation of tickets, ensuring that the appropriate updates are made to flight seat availability.

- `DatabaseManager.java`: Manages the database connection and executes SQL queries. This class handles the connection to the MySQL database and provides methods for executing SQL queries.

## Screenshots

![Screenshot 1](screenshots/sc1.png)
![Screenshot 2](screenshots/sc2.png)
![Screenshot 3](screenshots/sc3.png)
![Screenshot 4](screenshots/sc4.png)
![Screenshot 5](screenshots/sc5.png)
![Screenshot 6](screenshots/sc6.png)
![Screenshot 7](screenshots/sc7.png)


