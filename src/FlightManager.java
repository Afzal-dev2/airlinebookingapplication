import java.sql.*;
import java.util.Scanner;

public class FlightManager {
    private DatabaseManager databaseManager;

    public FlightManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public void insertFlightDetails(Scanner scanner) {
        try {
            System.out.println("Enter flight name: ");
            String flightName = scanner.nextLine();
            System.out.println("Enter source: ");
            String source = scanner.nextLine();
            System.out.println("Enter destination: ");
            String destination = scanner.nextLine();
            System.out.println("Enter total seats: ");
            int totalSeats = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.println("Enter vacant seats: ");
            int vacantSeats = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            Connection connection = databaseManager.getConnection();
            String query = "INSERT INTO flights (flight_name, source, destination, total_seats, vacant_seats) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, flightName);
            preparedStatement.setString(2, source);
            preparedStatement.setString(3, destination);
            preparedStatement.setInt(4, totalSeats);
            preparedStatement.setInt(5, vacantSeats);
            preparedStatement.executeUpdate();

            PrintHighlighted.printHighlighted("Flight details inserted successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateFlightDetails(Scanner scanner) {
        try {
            System.out.println("Enter flight Id: ");
            int flightId = scanner.nextInt();
            Connection connection = databaseManager.getConnection();
            String checkFlightQuery = "SELECT * FROM flights WHERE id = ?";
            PreparedStatement checkFlightStmt = connection.prepareStatement(checkFlightQuery);
            checkFlightStmt.setInt(1, flightId);
            ResultSet rs = checkFlightStmt.executeQuery();

            if (rs.next()) {
                System.out.println("Enter updated flight name: ");
                String flightName = scanner.next();
                System.out.println("Enter updated source: ");
                String source = scanner.next();
                System.out.println("Enter updated destination: ");
                String destination = scanner.next();
                System.out.println("Enter updated total seats: ");
                int totalSeats = scanner.nextInt();
                scanner.nextLine(); // Consume newline
                System.out.println("Enter updated vacant seats: ");
                int vacantSeats = scanner.nextInt();
                scanner.nextLine(); // Consume newline

                // Check if there are booked tickets for this flight
                String checkBookingsQuery = "SELECT COUNT(*) AS num_bookings FROM bookings WHERE flight_id = ?";
                PreparedStatement checkBookingsStmt = connection.prepareStatement(checkBookingsQuery);
                checkBookingsStmt.setInt(1, flightId);
                ResultSet bookingsResult = checkBookingsStmt.executeQuery();
                if (bookingsResult.next()) {
                    int numBookings = bookingsResult.getInt("num_bookings");
                    if (numBookings > totalSeats) {
                        PrintHighlighted.printHighlighted("Cannot update total seats. Booked tickets exceed the new total seats.");
                        return;
                    }
                }

                String updateFlightQuery = "UPDATE flights SET flight_name = ?, source = ?, destination = ?, total_seats = ?, vacant_seats = ? WHERE id = ?";
                PreparedStatement updateFlightStmt = connection.prepareStatement(updateFlightQuery);
                updateFlightStmt.setString(1, flightName);
                updateFlightStmt.setString(2, source);
                updateFlightStmt.setString(3, destination);
                updateFlightStmt.setInt(4, totalSeats);
                updateFlightStmt.setInt(5, vacantSeats);
                updateFlightStmt.setInt(6, flightId);
                updateFlightStmt.executeUpdate();

                PrintHighlighted.printHighlighted("Flight details updated successfully.");
            } else {
                PrintHighlighted.printHighlighted("Flight with Name " + flightId + " not found.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void displayVacantSeatDetails() {
        try {
            Connection connection = databaseManager.getConnection();
            String query = "SELECT id, flight_name, vacant_seats FROM flights WHERE vacant_seats > 0";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            System.out.println("Available Flights:");
            while (rs.next()) {
                PrintHighlighted.printHighlighted("Flight ID: " + rs.getInt("id") +
                        ", Flight Name: " + rs.getString("flight_name") +
                        ", Vacant Seats: " + rs.getInt("vacant_seats"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
