import java.util.Scanner;

public class AirlineTicketBookingApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DatabaseManager databaseManager = new DatabaseManager();
        PassengerManager passengerManager = new PassengerManager(databaseManager);
        FlightManager flightManager = new FlightManager(databaseManager);
        BookingManager bookingManager = new BookingManager(databaseManager);

        while (true) {
            displayMenu();
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    passengerManager.insertPassengerDetails(scanner);
                    break;
                case 2:
                    flightManager.insertFlightDetails(scanner);
                    break;
                case 3:
                    bookingManager.bookTicket(scanner);
                    break;
                case 4:
                    bookingManager.cancelTicket(scanner);
                    break;
                case 5:
                    flightManager.displayVacantSeatDetails();
                    break;
                case 6:
                    passengerManager.updatePassengerInformation(scanner);
                    break;
                case 7:
                    flightManager.updateFlightDetails(scanner);
                    break;
                case 8:
                    System.exit(0);
                default:
                    PrintHighlighted.printHighlighted("Invalid choice. Please enter a number from 1 to 8.");
            }
        }
    }


    private static void displayMenu() {
        System.out.println("Airline Ticket Booking Application");
        System.out.println("1. Insert Passenger Details");
        System.out.println("2. Insert Flight Details");
        System.out.println("3. Book Ticket");
        System.out.println("4. Cancel Ticket");
        System.out.println("5. Display Vacant Seat Details");
        System.out.println("6. Update Passenger Information");
        System.out.println("7. Update Flight Details");
        System.out.println("8. Exit");
        System.out.print("Enter your choice: ");
    }

}
