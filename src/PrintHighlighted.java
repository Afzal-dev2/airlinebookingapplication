// utility class to print in yellow color
public class PrintHighlighted {
    public static void printHighlighted(String message) {
        // ANSI escape code for yellow text color
        String ANSI_YELLOW = "\u001B[33m";
        // ANSI escape code for reset (back to default) text color
        String ANSI_RESET = "\u001B[0m";

        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }
}
