import java.sql.*;
import java.util.Scanner;

public class BookingManager {
    private final DatabaseManager databaseManager;

    public BookingManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public void bookTicket(Scanner scanner) {
        try {
            System.out.println("Enter passenger name: ");
            String passengerName = scanner.nextLine();

            // Find passenger ID by name
            int passengerId = findPassengerIdByName(passengerName);
            if (passengerId == -1) {
                PrintHighlighted.printHighlighted("Passenger not found.");
                return;
            }

            // Display available flights
            boolean isAvailable = displayAvailableFlights();
            if (!isAvailable) {
                return;
            }
            // Prompt user to select a flight
            System.out.println("Enter flight ID to book: ");
            int flightId = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            // Check if flight ID is valid
            if (!isFlightIdValid(flightId)) {
                PrintHighlighted.printHighlighted("Invalid flight ID.");
                return;
            }

            // Check if flight has available seats
            if (!hasAvailableSeats(flightId)) {
                PrintHighlighted.printHighlighted("No available seats on this flight.");
                return;
            }

            // Book the ticket
            bookTicket(passengerId, flightId);

            PrintHighlighted.printHighlighted("Ticket booked successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void cancelTicket(Scanner scanner) {
        try {
            System.out.println("Enter passenger name: ");
            String passengerName = scanner.nextLine();

            // Find bookings for the given passenger
            int passengerId = findPassengerIdByName(passengerName);
            if (passengerId == -1) {
                PrintHighlighted.printHighlighted("No bookings found for the passenger.");
                return;
            }

            // Display bookings for the passenger
            displayBookingsByPassenger(passengerId);

            // Prompt user to select a booking to cancel
            System.out.println("Enter booking ID to cancel: ");
            int bookingId = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            // Check if the entered booking ID is valid
            if (!isBookingIdValid(bookingId, passengerId)) {
                System.out.println("Invalid booking ID.");
                return;
            }

            // Cancel the ticket
            cancelTicket(bookingId);

            PrintHighlighted.printHighlighted("Ticket canceled successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int findPassengerIdByName(String passengerName) throws SQLException {
        Connection connection = databaseManager.getConnection();
        String query = "SELECT id FROM passengers WHERE name = ?";
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setString(1, passengerName);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            return rs.getInt("id");
        }
        return -1; // Passenger not found
    }

    private boolean displayAvailableFlights() throws SQLException {
        Connection connection = databaseManager.getConnection();
        String query = "SELECT * FROM flights WHERE vacant_seats > 0";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);

//        if there are no flights with vacant seats,
//        the user will be informed that there are no available flights
        if (!areFlightsAvailable()) {
            PrintHighlighted.printHighlighted("No available flights.");
            return false;
        }
        System.out.println("Available Flights:");

        while (rs.next()) {
            PrintHighlighted.printHighlighted("Flight ID: " + rs.getInt("id")
                    + ", Source: " + rs.getString("source") + ", Destination: " + rs.getString("destination") +
                    ", Vacant Seats: " + rs.getInt("vacant_seats")
            );
        }
        return true;
    }
    public boolean areFlightsAvailable() {
        try {
            Connection connection = databaseManager.getConnection();
            String query = "SELECT COUNT(*) AS available_flights FROM flights WHERE vacant_seats > 0";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            if (resultSet.next()) {
                int availableFlightsCount = resultSet.getInt("available_flights");
                return availableFlightsCount > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false; // Return false by default or handle the error appropriately
    }
    private boolean isFlightIdValid(int flightId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        String query = "SELECT id FROM flights WHERE id = ?";
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, flightId);
        ResultSet rs = pstmt.executeQuery();
        return rs.next(); // true if flight ID exists, false otherwise
    }

    private boolean hasAvailableSeats(int flightId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        String query = "SELECT vacant_seats FROM flights WHERE id = ?";
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, flightId);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            int vacantSeats = rs.getInt("vacant_seats");
            return vacantSeats > 0;
        }
        return false; // Flight not found
    }

    private void bookTicket(int passengerId, int flightId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        // Decrement vacant seats
        String updateVacantSeatsQuery = "UPDATE flights SET vacant_seats = vacant_seats - 1 WHERE id = ?";
        PreparedStatement updateVacantSeatsStmt = connection.prepareStatement(updateVacantSeatsQuery);
        updateVacantSeatsStmt.setInt(1, flightId);
        updateVacantSeatsStmt.executeUpdate();

        // Insert booking information
        String insertBookingQuery = "INSERT INTO bookings (passenger_id, flight_id) VALUES (?, ?)";
        PreparedStatement insertBookingStmt = connection.prepareStatement(insertBookingQuery);
        insertBookingStmt.setInt(1, passengerId);
        insertBookingStmt.setInt(2, flightId);
        insertBookingStmt.executeUpdate();
    }

    private void displayBookingsByPassenger(int passengerId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        System.out.println("Bookings for Passenger " + passengerId + ":");
        String query = "SELECT id FROM bookings WHERE passenger_id = ?";
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, passengerId);
        ResultSet rs = pstmt.executeQuery();
        while (rs.next()) {
            System.out.println("Booking ID: " + rs.getInt("id"));
        }
    }

    private boolean isBookingIdValid(int bookingId, int passengerId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        String query = "SELECT id FROM bookings WHERE id = ? AND passenger_id = ?";
        PreparedStatement pstmt = connection.prepareStatement(query);
        pstmt.setInt(1, bookingId);
        pstmt.setInt(2, passengerId);
        ResultSet rs = pstmt.executeQuery();
        return rs.next(); // true if booking ID is valid for the passenger, false otherwise
    }

    private void cancelTicket(int bookingId) throws SQLException {
        Connection connection = databaseManager.getConnection();
        // Get flight ID from booking
        String getFlightIdQuery = "SELECT flight_id FROM bookings WHERE id = ?";
        PreparedStatement getFlightIdStmt = connection.prepareStatement(getFlightIdQuery);
        getFlightIdStmt.setInt(1, bookingId);
        ResultSet rs = getFlightIdStmt.executeQuery();
        if (rs.next()) {
            int flightId = rs.getInt("flight_id");

            // Increment vacant seats
            String updateVacantSeatsQuery = "UPDATE flights SET vacant_seats = vacant_seats + 1 WHERE id = ?";
            PreparedStatement updateVacantSeatsStmt = connection.prepareStatement(updateVacantSeatsQuery);
            updateVacantSeatsStmt.setInt(1, flightId);
            updateVacantSeatsStmt.executeUpdate();

            // Delete booking
            String deleteBookingQuery = "DELETE FROM bookings WHERE id = ?";
            PreparedStatement deleteBookingStmt = connection.prepareStatement(deleteBookingQuery);
            deleteBookingStmt.setInt(1, bookingId);
            deleteBookingStmt.executeUpdate();
        }

    }
}
