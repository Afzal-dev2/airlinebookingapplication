import java.sql.*;
import java.util.Scanner;

public class PassengerManager {
    private final DatabaseManager databaseManager;

    public PassengerManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public void insertPassengerDetails(Scanner scanner) {
        try {
            System.out.println("Enter passenger name: ");
            String name = scanner.nextLine();
            System.out.println("Enter age: ");
            int age = scanner.nextInt();
            scanner.nextLine(); // Consume newline
            System.out.println("Enter gender: ");
            String gender = scanner.nextLine();
            System.out.println("Enter contact number: ");
            String contact = scanner.nextLine();
            System.out.println("Enter Aadhar number: ");
            String aadharNumber = scanner.nextLine(); // Added Aadhar Number field

            Connection connection = databaseManager.getConnection();
            String query = "INSERT INTO passengers (name, age, gender, contact, aadhar_number) VALUES (?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, age);
            preparedStatement.setString(3, gender);
            preparedStatement.setString(4, contact);
            preparedStatement.setString(5, aadharNumber); // Added Aadhar Number field
            preparedStatement.executeUpdate();

            PrintHighlighted.printHighlighted("Passenger details inserted successfully.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePassengerInformation(Scanner scanner) {
        try {
            System.out.println("Enter passenger Name: ");
            String passengerName = scanner.nextLine();

            Connection connection = databaseManager.getConnection();
            String checkPassengerQuery = "SELECT * FROM passengers WHERE name = ?";
            PreparedStatement checkPassengerStmt = connection.prepareStatement(checkPassengerQuery);
            checkPassengerStmt.setString(1, passengerName);
            ResultSet rs = checkPassengerStmt.executeQuery();

            if (rs.next()) {
                System.out.println("Enter updated name: ");
                String name = scanner.nextLine();
                System.out.println("Enter updated age: ");
                int age = scanner.nextInt();
                scanner.nextLine(); // Consume newline
                System.out.println("Enter updated contact: ");
                String contact = scanner.nextLine();

                String updatePassengerQuery = "UPDATE passengers SET name = ?, age = ?, contact = ? WHERE name = ?";
                PreparedStatement updatePassengerStmt = connection.prepareStatement(updatePassengerQuery);
                updatePassengerStmt.setString(1, name);
                updatePassengerStmt.setInt(2, age);
                updatePassengerStmt.setString(3, contact);
                updatePassengerStmt.setString(4, passengerName);
                updatePassengerStmt.executeUpdate();

                PrintHighlighted.printHighlighted("Passenger information updated successfully.");
            } else {
                PrintHighlighted.printHighlighted("Passenger with Name " + passengerName + " not found.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
